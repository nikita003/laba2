package org.grechishnikov.laba2;

public interface Sender {
    public String sendMessage(String message);
}
